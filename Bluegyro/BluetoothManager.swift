//
//  BluetoothManager.swift
//  Bluegyro
//
//  Created by Serge Sinkevych on 1/21/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import CoreBluetooth

class BluetoothManager: NSObject {
    
    static let shared = BluetoothManager()
    weak var delegate: BluetoothDelegate?
    
    var centralManager: CBCentralManager?
    var peripheralManager: CBPeripheralManager?
    var cacheDevices: [CBPeripheral] = []
    var discoveredDevices: [CBPeripheral] = []
    var connectedPeripheral: CBPeripheral?
    var mainBatteryService: CBService?
    var isScanning: Bool {
        return self.centralManager?.isScanning ?? false
    }
}

// MARK: - Setup -

extension BluetoothManager {
    
    func setup() {
        if centralManager == nil {
            self.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
        }
        if peripheralManager == nil {
            self.peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        }
    }
    
    func startScanning() {
        discoveredDevices.removeAll()
        startScan()
    }
    
    func stopScanning() {
        stopScan()
    }
    
    func connectDevice(with uuidString: String) {
        guard let manager = self.centralManager else { return }
        guard let uuid = UUID(uuidString: uuidString) else { return }
        guard let peripheral = manager.retrievePeripherals(withIdentifiers: [uuid]).first else { return }
        manager.connect(peripheral, options: nil)
    }
    
    func disconnectDevice(with uuidString: String) {
        guard let manager = self.centralManager else { return }
        guard let uuid = UUID(uuidString: uuidString) else { return }
        guard let peripheral = manager.retrievePeripherals(withIdentifiers: [uuid]).first else { return }
        manager.cancelPeripheralConnection(peripheral)
    }
}

// MARK: - Helpers -

extension BluetoothManager {
   
    private func startScan() {
        guard let manager = centralManager else { return }
        discoveredDevices.removeAll()
        manager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        
    }

    private func stopScan() {
        guard let manager = centralManager else { return }
        manager.stopScan()
    }
}

// MARK: - Central Manager Delegate -

extension BluetoothManager: CBCentralManagerDelegate {
    internal func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        guard let advertisedName = advertisementData["kCBAdvDataLocalName"] as? String else {
            return
        }
        print("Discovered: \(advertisedName), RSSI: \(RSSI)\n")
        if advertisedName.contains("CIRCU") {
            self.delegate?.blueLog(text: "Discovered CIRCU")
            central.connect(peripheral, options: nil)
            stopScan()
        }
        discoveredDevices.append(peripheral)
    }
    
    internal func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected to: ", peripheral)
        connectedPeripheral = peripheral
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        self.delegate?.blueLog(text: "Connected CIRCU")
    }
    
    internal func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Failed to connect")
    }
    
    internal func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Did disconnect")
    }
    
    internal func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print(central.state)
    }
}

extension BluetoothManager: CBPeripheralDelegate, CBPeripheralManagerDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("Update notification for characteristic: \(characteristic)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services, error == nil else {
            return
        }
        for service: CBService in services {
            print("Service found: ", service)
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state != .poweredOn {
            print("Peripheral Power off")
            return
        }
        if peripheral.state == .poweredOn {
            print("Peripheral Power on")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics, error == nil else {
            return
        }
        for char in characteristics {
            if char.uuid.uuidString == Constants.rx_char {
                peripheral.setNotifyValue(true, for: char)
            }
        }
        print("Did discover characteristic: ", characteristics)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Did write value for")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Did update value: ")
        if let data = characteristic.value {
            print(String(data: data, encoding: .ascii) ?? "no data")
        }
    }
}

