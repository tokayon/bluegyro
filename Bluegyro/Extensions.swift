//
//  Extensions.swift
//  Bluegyro
//
//  Created by Serge Sinkevych on 7/30/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import SceneKit


extension UIColor {
    
    static var lightBlue: UIColor { get { return hexColor(hex: "#96B9E1", 1)}}
    static var mediumBlue: UIColor { get { return hexColor(hex: "#84A3C7", 1)}}
    static var darkBlue: UIColor { get { return hexColor(hex: "#4B5D7D", 1)}}
    static var ultraDarkBlue: UIColor { get { return hexColor(hex: "#354157", 1)}}
    static var sand: UIColor { get { return hexColor(hex: "#C28648", 1)}}
    static var silver: UIColor { get { return hexColor(hex: "#C9C6C6", 1)}}

    
    static var flipsideBlue: UIColor { get { return hexColor(hex: "#1F2124", 1)}}
    
    
    private class func hexColor(hex:String, _ alpha: CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}

extension SCNNode {
    
    var specularColor: UIColor? {
        get { return self.geometry?.firstMaterial?.specular.contents as? UIColor }
        set { self.geometry?.firstMaterial?.specular.contents = newValue }
    }
    
    func setupColors(front: UIColor, right: UIColor, back: UIColor, left: UIColor, top: UIColor, bottom: UIColor) {
        let colors = [front, right, back, left, top, bottom]
        let colorMaterials = colors.map { color -> SCNMaterial in
            let material = SCNMaterial()
            material.diffuse.contents = color
            material.locksAmbientWithDiffuse = true
            return material
        }
        self.geometry?.materials = colorMaterials
    }
}
