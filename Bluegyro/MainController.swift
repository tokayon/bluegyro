//
//  MainController.swift
//  Bluegyro
//
//  Created by Serge Sinkevych on 7/21/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import SceneKit

class MainController: UIViewController {

    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sceneView: SCNView!
    
    
    // Geometry
    var geometryNode: SCNNode = SCNNode()
    
    // Gestures
    var currentAngle: Float = 0.0
    
    var bluetoothManager: BluetoothManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
        
    }
    
    @IBAction func connect(_ sender: UIButton) {
        bluetoothManager?.startScanning()
        addLog(text: "Start scanning...")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Setup -

extension MainController {
    func initialSettings() {
        bluetoothManager = BluetoothManager.shared
        bluetoothManager?.delegate = self
        bluetoothManager?.setup()
        sceneSetup()
        statusLabel.isHidden = true
    }
    
    func addLog(text: String) {
        statusLabel.isHidden = false
        statusLabel.text = "Status: " + text
    }
    
    func sceneSetup() {
        let scene = SCNScene()
        let mb = UIColor.mediumBlue
        let lb = UIColor.lightBlue
        let db = UIColor.darkBlue
        let ub = UIColor.ultraDarkBlue
        
        let bottomBox = NodeManager.box(width: 5.0, height: 1.0, length: 5.0)
        bottomBox.setupColors(front: db, right: db, back: db, left: db, top: db, bottom: ub)
        scene.rootNode.addChildNode(bottomBox)
        
        let topBox = NodeManager.box(width: 5.0, height: 1.0, length: 5.0)
        topBox.setupColors(front: mb, right: mb, back: mb, left: mb, top: lb, bottom: mb)
        topBox.position = SCNVector3Make(0, 1, 0)
        bottomBox.addChildNode(topBox)
        
        let charger = NodeManager.box(width: 1.0, height: 0.2, length: 1.0, color: UIColor.silver)
        charger.position = SCNVector3Make(-2, 0.5, 0)
        charger.specularColor = UIColor.white
        topBox.addChildNode(charger)
        
        let processor = NodeManager.box(width: 2.0, height: 0.6, length: 2.0, color: UIColor.darkGray)
        processor.position = SCNVector3Make(0, 0.5, 0)
        processor.specularColor = UIColor.white
        topBox.addChildNode(processor)
        
        let lfcylinder = NodeManager.cylinder(radius: 0.3, height: 0.1, color: UIColor.sand)
        lfcylinder.position = SCNVector3Make(-2.0, 0.5, 2.0)
        lfcylinder.specularColor = UIColor.white
        topBox.addChildNode(lfcylinder)
        
        let rfcylinder = NodeManager.cylinder(radius: 0.3, height: 0.1, color: UIColor.sand)
        rfcylinder.position = SCNVector3Make(2.0, 0.5, 2.0)
        rfcylinder.specularColor = UIColor.white
        topBox.addChildNode(rfcylinder)
        
        let lbcylinder = NodeManager.cylinder(radius: 0.3, height: 0.1, color: UIColor.sand)
        lbcylinder.position = SCNVector3Make(-2.0, 0.5, -2.0)
        lbcylinder.specularColor = UIColor.white
        topBox.addChildNode(lbcylinder)
        
        let rbcylinder = NodeManager.cylinder(radius: 0.3, height: 0.1, color: UIColor.sand)
        rbcylinder.position = SCNVector3Make(2.0, 0.5, -2.0)
        rbcylinder.specularColor = UIColor.white
        topBox.addChildNode(rbcylinder)
        
        geometryNode = bottomBox

        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3Make(0, 0, 10)
        scene.rootNode.addChildNode(cameraNode)
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light?.type = SCNLight.LightType.ambient
        lightNode.light?.color = UIColor(white: 0.8, alpha: 1.0)
        lightNode.position = SCNVector3Make(0, 10, 5)
        scene.rootNode.addChildNode(lightNode)
        
        sceneView.backgroundColor = UIColor.flipsideBlue
        sceneView.scene = scene
        sceneView.autoenablesDefaultLighting = true
        sceneView.allowsCameraControl = true
    }
    
    func addPanGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGesture(sender:)))
        sceneView.addGestureRecognizer(pan)
    }
    
    @objc func panGesture(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: sender.view!)
        var newAngle = (Float)(translation.x)*(Float)(Double.pi)/180.0
        newAngle += currentAngle
        
        geometryNode.transform = SCNMatrix4MakeRotation(newAngle, 0, 0, 1)
        
        if(sender.state == UIGestureRecognizer.State.ended) {
            currentAngle = newAngle
        }
    }
  
}

extension MainController: BluetoothDelegate {
    func blueLog(text: String) {
        addLog(text: text)
    }
    
}



