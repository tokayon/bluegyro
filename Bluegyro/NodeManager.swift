//
//  NodeManager.swift
//  Bluegyro
//
//  Created by Serge Sinkevych on 7/31/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation
import SceneKit

class NodeManager: NSObject {
    
    class func box(width: CGFloat, height: CGFloat, length: CGFloat, color: UIColor? = nil) -> SCNNode {
        let box = SCNBox(width: width, height: height, length: length, chamferRadius: 0.1)
        box.firstMaterial?.diffuse.contents = color
        return SCNNode(geometry: box)
    }
    
    class func sphere(radius: CGFloat, color: UIColor, specular: UIColor? = nil) -> SCNNode {
        let sphere = SCNSphere(radius: radius)
        sphere.firstMaterial?.diffuse.contents = color
        sphere.firstMaterial?.specular.contents = specular
        return SCNNode(geometry: sphere)
    }
    
    class func cylinder(radius: CGFloat, height: CGFloat, color: UIColor) -> SCNNode {
        let cylinder = SCNCylinder(radius: radius, height: radius)
        cylinder.firstMaterial?.diffuse.contents = color
        return SCNNode(geometry: cylinder)
    }
}
