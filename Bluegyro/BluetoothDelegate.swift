//
//  BluetoothDelegate.swift
//  Bluegyro
//
//  Created by Serge Sinkevych on 7/21/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

protocol BluetoothDelegate: class {
    func blueLog(text: String)
}
