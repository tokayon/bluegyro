//
//  Constants.swift
//  Bluegyro
//
//  Created by Serge Sinkevych on 7/21/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation


struct Constants {
    static let tx_char = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
    static let rx_char = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
}
